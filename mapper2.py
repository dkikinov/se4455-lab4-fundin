#!/usr/bin/env python

"""mapper.py"""

import sys

# input comes from stdin
for line in sys.stdin:
    # remove leading and trailing whitespaces
    line = line.strip()
    # split the line into the individual columns
    data_columns = line.split("\t")

    # remove unneeded elements in the list (i.e., date, time, type, and payment method)
    # keeping only the store/city and the amount
    data_columns.pop(0)
    data_columns.pop(0)
    data_columns.pop(0)
    data_columns.pop(0)

    print "%s\t%s" % (data_columns[1], data_columns[0])