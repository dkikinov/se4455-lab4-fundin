#!/usr/bin/env python

"""reducer.py"""

import sys

current_payment_method = None
current_total = 0.0
payment_method = None
payment_method_total_list = []
large = 0.0
max_index = 0
rank_list = []


for line in sys.stdin:
    # remove leading and trailing whitespaces
    line = line.strip()
    # split the incoming data into the store and puchase amount
    payment_method, purchase_amount = line.split("\t", 1)
    # convert the purchase amount into a float
    try:
        purchase_amount = float(purchase_amount)
    except ValueError:
        # the purchases amount was not a number
        # discard this set of data as it was incorrect
        continue
    if current_payment_method == payment_method:
        current_total += purchase_amount
    else:
        if current_payment_method:
            # save the result in the list
            payment_method_total_list.append(tuple((current_payment_method, current_total)))
        current_payment_method = payment_method
        current_total = purchase_amount

# to rank and output rank results
if current_payment_method == payment_method:
    payment_method_total_list.append(tuple((current_payment_method, current_total)))
    index = -1
    while len(payment_method_total_list) > 0:
        large = 0.0
        index = -1
        max_index = 0
        for n in payment_method_total_list:
            index += 1
            if n[1] > large:
                max_index = index
                large = n[1]
        rank_list.append(payment_method_total_list[max_index][0])
        payment_method_total_list.pop(max_index)
    index = 0
    for r in rank_list:
        index += 1
        print "%s.\t%s" % (index, r)
