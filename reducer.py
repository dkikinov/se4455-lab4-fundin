#!/usr/bin/env python

"""reducer.py"""

import sys

current_store = None
current_total = 0.0
store = None

for line in sys.stdin:
    # remove leading and trailing whitespaces
    line = line.strip()
    # split the incoming data into the store and puchase amount
    store, purchase_amount = line.split("\t", 1)
    # convert the purchase amount into a float
    try:
        purchase_amount = float(purchase_amount)
    except ValueError:
        # the purchases amount was not a number
        # discard this set of data as it was incorrect
        continue
    if current_store == store:
        current_total += purchase_amount
    else:
        if current_store:
            # output the result
            print "%s\t%s" % (current_store, current_total)
        current_store = store
        current_total = purchase_amount

# to output the last word
if current_store == store:
    print "%s\t%s" % (current_store, current_total)
